package ru.krivotulov.tm.api.controller;

public interface ICommandController {

    void displayArguments();

    void displayCommands();

    void displayHelp();

    void displaySystemInfo();

    void displayVersion();

    void displayAbout();

    void displayError(String arg);

}
