package ru.krivotulov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! id is empty...");
    }

}
