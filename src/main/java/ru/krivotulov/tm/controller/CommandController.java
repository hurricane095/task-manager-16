package ru.krivotulov.tm.controller;

import ru.krivotulov.tm.api.controller.ICommandController;
import ru.krivotulov.tm.api.service.ICommandService;
import ru.krivotulov.tm.model.Command;
import ru.krivotulov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void displayCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void displaySystemInfo() {
        Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryFormat);
    }

    @Override
    public void displayVersion() {
        System.out.println("1.9.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Aleksey Krivotulov");
        System.out.println("akrivotulov@tsconsulting.com");
    }

    @Override
    public void displayError(String arg) {
        System.err.printf("Error! This argument `%s` not supported... \n", arg);
    }

}
